import 'package:flutter/material.dart';

class SkillCard extends StatefulWidget {
  const SkillCard({Key key}) : super(key: key);

  @override
  _SkillCardState createState() => _SkillCardState();
}

class _SkillCardState extends State<SkillCard> {
  bool toggleState = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(
          Radius.circular(15.0)
      ),
      //color: Colors.blue,
      color: Colors.white70,
      child: InkWell(
        onTap: toggle, // handle your onTap here
        child: Container(
          width: 150,
          height: 250,
          child: Card(
            color: Colors.white70,
            elevation: 10,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                  Radius.circular(15.0)
              ),
            ),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
              decoration: myBoxDecoration(),
              child: myCardContent(),
            ),
          ),
        ),
      ),
    );

    /*return Container(
      width: 150,
      height: 250,
      child: Card(
        color: Colors.white70,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
              Radius.circular(15.0)
          ),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
          decoration: myBoxDecoration(),
          child: myCardContent(),
        ),
      ),
    );*/

    /*return Container(
      width: 200,
      decoration: myBoxDecoration(),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Colors.pink,
        elevation: 10,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.album, size: 70),
              title: Text(
                  'Heart Shaker', style: TextStyle(color: Colors.white)),
              subtitle: Text('TWICE', style: TextStyle(color: Colors.white)),
            ),
            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: const Text(
                        'Edit', style: TextStyle(color: Colors.white)),
                    onPressed: () {},
                  ),
                  FlatButton(
                    child: const Text(
                        'Delete', style: TextStyle(color: Colors.white)),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );*/
  }

  Column myCardContent(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      //mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        header(),
        separator(),
        footer(),
      ],
    );
  }
  /*RaisedButton myCardContent(){
    return RaisedButton(
      onPressed: toggle,
      //color: Colors.red,
      //splashColor: Colors.grey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        //mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          header(),
          separator(),
          footer(),
        ],
      ),
    );
  }*/

  Container header(){
    return Container(
      width: 130,
      height: 70,
      //color: Colors.amberAccent,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Icon(Icons.album, color: toggleState? Colors.black : Colors.blue, size: 45),
          Text('Dancing', style: TextStyle(color: Colors.black)),
          //Text('TWICE', style: TextStyle(color: Colors.white)),
        ],
      ),
    );
  }

  Container separator(){
    return Container(
      width: 130,
      height: 2,
      color: Colors.black38,
    );
  }

  Container footer(){
    return Container(
      width: 130,
      height: 150,
      //color: Colors.deepOrangeAccent,
        child: Text("I feel like dancing, dancing, paosndasndlasn. I feel like dancing, dancing. asdbasdhbasjhd you -o -o.", style: TextStyle(color: Colors.black)),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
          width: 1.5,
          color: toggleState? Colors.black : Colors.blue
      ),
      borderRadius: BorderRadius.all(
          Radius.circular(15.0) //         <--- border radius here
      ),
    );
  }

  toggle(){
    print(toggleState);

    setState(() {
      toggleState = !toggleState;
    });
  }
}
