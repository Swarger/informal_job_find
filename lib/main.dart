import 'package:flutter/material.dart';
import 'package:informal_job_find/models/user.dart';
import 'package:informal_job_find/screens/landingWrapper.dart';
import 'package:informal_job_find/services/auth.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService()
          .user, // This is the Stream we are listening to. Allows us to listen to Stream changes
      child: MaterialApp(
        home: LandingWrapper(),
      ),
    );
  }
}
