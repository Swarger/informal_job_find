import 'package:flutter/material.dart';
import 'package:informal_job_find/screens/authenticate/sign_in.dart';

import 'package:informal_job_find/screens/home/home.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  @override
  Widget build(BuildContext context) {
    /*return Container(
      child: SignIn(onSignIn: () => print('login successful!'),),
    );*/
    return Container(
      child: SignIn(onSignIn: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Home())),),
    );
  }
}
