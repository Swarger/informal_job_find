import 'package:flutter/material.dart';
import 'package:informal_job_find/services/auth.dart';

// This is just my tests
import 'package:informal_job_find/components/skill_card.dart';
// This is just my tests

class Home extends StatelessWidget {
  final _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.brown[50],
        appBar: AppBar(
          title: Text('Informal Job Finder'),
          backgroundColor: Colors.brown[400],
          elevation: 0.0,
          actions: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.person),
              label: Text('Logout'),
              onPressed: () async {
                // Will trigger Stream to change, which will prompt provider to send null to all listeners
                await _auth.signOut();
              },
            ),
          ],
        ),
        body: SkillCard(),
      ),
    );
  }
}
