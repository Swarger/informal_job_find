import 'package:flutter/material.dart';
import 'package:informal_job_find/screens/authenticate/authenticate.dart';
import 'package:informal_job_find/screens/home/home.dart';
import 'package:informal_job_find/models/user.dart';

// Test
import 'package:informal_job_find/services/auth.dart';
// Test

/// Directs the user to the Home screen if the user is logged in,
/// otherwise directs the user to the login/signup screen

class LandingWrapper extends StatefulWidget {
  @override
  _LandingWrapperState createState() => _LandingWrapperState();
}

class _LandingWrapperState extends State<LandingWrapper> {
  AuthService _auth;

  @override
  void initState() {
    super.initState();
    _auth = AuthService();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Informal Job Find',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Informal Job Find'),
        ),
        body: FutureBuilder<User>(
          future: _auth.getCurrentSignedInUser(),
          builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
            if(snapshot.connectionState == ConnectionState.waiting){
              // while data is loading:
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            else if(snapshot.connectionState == ConnectionState.done){
              if(snapshot.hasData){
                return Home();
              }
              else{
                return Authenticate();
              }
            }
            else{
              return Authenticate();
            }
            /*if (!snapshot.hasData) {
              // while data is loading:
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              // data loaded:
              final info = snapshot.data;
              if (info != null) {
                return Home();
              } else {
                return Authenticate();
              }
            }*/
          },
        ),
      ),
    );
  }
}

/*class LandingWrapper extends StatelessWidget {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    _auth.getCurrentSignedInUser();
    // Returns either Home or Authenticate widget
    return Authenticate();
  }
}*/
