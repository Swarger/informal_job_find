import 'package:firebase_auth/firebase_auth.dart';
import 'package:informal_job_find/models/user.dart';

/// Service that handles user Authentication
///
class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Create User (custom model) from FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  // Auth Change User Stream
  // ? A stream is basically a connection to some service that will notify you when stuff changes
  Stream<User> get user {
    // Converts FirebaseUser that is received from Stream, to a custom User object
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  // Sign In Anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      if (user != null) {
        return _userFromFirebaseUser(user);
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // Get current signed in user
  Future<User> getCurrentSignedInUser() async {
    try {
      FirebaseUser user = await _auth.currentUser();
      print(user);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //final FirebaseAuth

  // Sign in with Email/Password

  // Register with Email/Password

  // Logout
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
